# Discord Collab
## Objectives
- Get experience in a repo with multiple contributors
- Practice branching, merge etc.
- Get experience with tailwindcss (Side quest)
## Instructions
0. npm install to install node_modules
1. create personal folder in /src/ e.g (/src/mors)
2. create index.html (and index.css) in personal folder
3. Add name in list in /src/index.html and link to personal folder
4. Personalize personal site and practice some tailwind classes
5. use this command to compile: npx tailwindcss -i ./src/input.css -o ./dist/output.css --watch
6. check out your tailwind styled webpage
7. create a new branch (or not) and commit to git repository
8. lets talk in #general chat to merge the branches
## Steps Taken for Initial Commit
1. npm init
2. install tailwindcss (https://tailwindcss.com/docs/installation)
3. Edited src/index.html for instructions